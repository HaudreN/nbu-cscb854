import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Todo } from 'src/app/models/Todo'
import { Observable } from 'rxjs'
const httpOptions =
{
	headers : new HttpHeaders({
		'Content-Type' : 'application/json'
	})
}
@Injectable({
  providedIn: 'root'
})
export class TodoService {
 todosUrl:string = 'https://jsonplaceholder.typicode.com/todos/';
  constructor(public http:HttpClient) { }

getTodos() : Observable<Todo[]>
{
	return this.http.get<Todo[]>(this.todosUrl + '?_limit=5');

/*	[
		{
			id: 1,
			title: 'Todo One',
			completed:false
		},
		{
			id: 2,
			title: 'Todo Two',
			completed:true
		},
		{
			id: 3,
			title: 'Todo Three',
			completed:false
		}
		
	] */
}
deleteTodo(todo:Todo):Observable<Todo>
{
	
	const url = `${this.todosUrl}/${todo.id}`;
	return this.http.delete<Todo>(url,httpOptions);
}
addTodo(todo:Todo):Observable<Todo> {
    return this.http.post<Todo>(this.todosUrl, todo, httpOptions);
  }
	 toggleCompleted(todo:Todo):Observable<any>
{
	const url = `${this.todosUrl}/${todo.id}`;
	return this.http.put(url,todo, httpOptions);
}
}
