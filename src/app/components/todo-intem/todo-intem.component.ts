import { Component, OnInit, Input,EventEmitter,Output } from '@angular/core';
import {Todo} from 'src/app/models/Todo';
import { TodoService } from '../../services/todo.service';
@Component({
  selector: 'app-todo-intem',
  templateUrl: './todo-intem.component.html',
  styleUrls: ['./todo-intem.component.css']
})
export class TodoIntemComponent implements OnInit {
 @Input() todo: Todo;
@Output() deleteTodo:EventEmitter<Todo> = new EventEmitter();
  constructor(public todoService:TodoService) { }

  ngOnInit() {
  }


setClasses()
{
	let classes =
	{
		todo:true,
		'is-complete': this.todo.completed
	}
	
	return classes;
}
onToggle(todo)
{
	todo.completed = !todo.completed;
	console.log('The following ToDo was Togled: ' + this.todo.title);
	this.todoService.toggleCompleted(todo).subscribe(todo => console.log(todo));
	
}
onDelete(todo)
{
	this.deleteTodo.emit(todo);
	console.log('The following ToDo was Deleted	: ' + this.todo.title);
	
}
}
