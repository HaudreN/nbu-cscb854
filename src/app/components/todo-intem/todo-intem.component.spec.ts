import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoIntemComponent } from './todo-intem.component';

describe('TodoIntemComponent', () => {
  let component: TodoIntemComponent;
  let fixture: ComponentFixture<TodoIntemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoIntemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoIntemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
