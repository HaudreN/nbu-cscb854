import { NgModule } from '@angular/core';
import {  Routes, RouterModule } from '@angular/router';
import { TodosComponent } from '../components/todos/todos.component';
import { JQueryPartComponent } from '../component/pages/jquery-part/jquery-part.component';
const routes: Routes = 
[
  {
    path: '', component:TodosComponent,
  },
  {
    path: 'jQueryPart', component:JQueryPartComponent,
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
  
})
export class AppRoutingModule { }
