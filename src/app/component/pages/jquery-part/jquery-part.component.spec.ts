import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JQueryPartComponent } from './jquery-part.component';

describe('JQueryPartComponent', () => {
  let component: JQueryPartComponent;
  let fixture: ComponentFixture<JQueryPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JQueryPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JQueryPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
