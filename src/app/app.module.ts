import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { TodosComponent } from './components/todos/todos.component';
import { TodoIntemComponent } from './components/todo-intem/todo-intem.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { AddTodoComponent } from './components/add-todo/add-todo.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { JQueryPartComponent } from './component/pages/jquery-part/jquery-part.component';
@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    TodoIntemComponent,
    HeaderComponent,
    AddTodoComponent,
    DashboardComponent,
    JQueryPartComponent
  ],
  imports: [
    BrowserModule,
HttpClientModule,
  FormsModule,
  RouterModule,
  AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
